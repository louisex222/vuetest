const path = require("path");
const ESLintPlugin = require("eslint-webpack-plugin");
const { configure } = require("quasar/wrappers");
const env = require("quasar-dotenv").config();
module.exports = configure(function (ctx) {
  return {
    htmlVariables: {
      title: process.env.HTML_TITLE,
    },
    supportTS: false,
    // preFetch: true,
    boot: ["main", "i18n", "axios", "validate", "swalert"],
    css: ["~assets/css/reset.scss"],
    extras: [
      "material-icons", // optional, you are not bound to it
    ],
    build: {
      env: env,
      vueRouterMode: "hash", // available values: 'hash', 'history'
      minify: true,
      gzip: true,
      showProgress: false,
      gzip: {
        filename: "[path][base].gz",
      },
      chainWebpack(chain) {
        chain.resolve.alias
          .set("@src", path.resolve(__dirname, "src"))
          .set("@boot", path.resolve(__dirname, "src/boot"))
          .set("@assets", path.resolve(__dirname, "src/assets"))
          .set("@layouts", path.resolve(__dirname, "src/layouts"))
          .set("@components", path.resolve(__dirname, "src/components"))
          .set("@css", path.resolve(__dirname, "src/css"))
          .set("@pages", path.resolve(__dirname, "src/pages"))
          .set("@modules", path.resolve(__dirname, "src/modules"))
          .set("@mixins", path.resolve(__dirname, "src/mixins"))
          .set("@hooks", path.resolve(__dirname, "src/hooks"))
          .set("@utils", path.resolve(__dirname, "src/utils"))
          .set("@store", path.resolve(__dirname, "src/store"))
          .set("@router", path.resolve(__dirname, "src/router"));
        chain
          .plugin("eslint-webpack-plugin")
          .use(ESLintPlugin, [{ extensions: ["js", "vue"] }]);
      },
    },

    // Full list of options: https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-devServer
    devServer: {
      https: false,
      port: 8080,
      open: true, // opens browser window automatically
    },

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-framework
    framework: {
      config: {},
      iconSet: "material-icons", // Quasar icon set
      lang: process.env.DEFAULT_LANG, // Quasar language pack
      plugins: [],
    },

    animations: "all", // --- includes all animations
    // https://quasar.dev/options/animations

    // https://quasar.dev/quasar-cli/developing-ssr/configuring-ssr
    ssr: {
      pwa: false,

      // manualStoreHydration: true,
      // manualPostHydrationTrigger: true,

      prodPort: 3000, // The default port that the production server should use
      // (gets superseded if process.env.PORT is specified at runtime)

      maxAge: 1000 * 60 * 60 * 24 * 30,
      // Tell browser when a file from the server should expire from cache (in ms)

      chainWebpackWebserver(chain) {
        chain
          .plugin("eslint-webpack-plugin")
          .use(ESLintPlugin, [{ extensions: ["js"] }]);
      },

      middlewares: [
        ctx.prod ? "compression" : "",
        "render", // keep this as last one
      ],
    },

    // https://quasar.dev/quasar-cli/developing-pwa/configuring-pwa
    pwa: {
      workboxPluginMode: "GenerateSW", // 'GenerateSW' or 'InjectManifest'
      workboxOptions: {}, // only for GenerateSW

      // for the custom service worker ONLY (/src-pwa/custom-service-worker.[js|ts])
      // if using workbox in InjectManifest mode
      chainWebpackCustomSW(chain) {
        chain
          .plugin("eslint-webpack-plugin")
          .use(ESLintPlugin, [{ extensions: ["js"] }]);
      },

      manifest: {
        name: `Quasar App`,
        short_name: `Quasar App`,
        description: `A Quasar Framework app`,
        display: "standalone",
        orientation: "portrait",
        background_color: "#ffffff",
        theme_color: "#027be3",
        icons: [
          {
            src: "icons/icon-128x128.png",
            sizes: "128x128",
            type: "image/png",
          },
          {
            src: "icons/icon-192x192.png",
            sizes: "192x192",
            type: "image/png",
          },
          {
            src: "icons/icon-256x256.png",
            sizes: "256x256",
            type: "image/png",
          },
          {
            src: "icons/icon-384x384.png",
            sizes: "384x384",
            type: "image/png",
          },
          {
            src: "icons/icon-512x512.png",
            sizes: "512x512",
            type: "image/png",
          },
        ],
      },
    },
  };
});
