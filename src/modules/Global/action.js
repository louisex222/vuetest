/** @format */
import {} from "./api";
import * as global_moduleTypes from "@store/root/mutationsTypes";
export const actions = {
  async globalAcChangeNavFlag({ commit }) {
    try {
      commit(global_moduleTypes.GLOBAL_CHANGE_NAVFLAG, "", { root: true });
    } catch (error) {
      return error;
    }
  },
};
