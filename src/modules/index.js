/** @format */
import crmGlobal from "./Global/index";
import crmDashboard from "./Dashboard/index";
import crmAuthUser from "./AuthUser/index";
import crmAuthRole from "./AuthRole/index";
import crmLogin from "./Login/index";
import crmCopy from "./Copy/index";
import crmEdit from "./Edit/index";
import crmQuestion from "./Question/index";
import crmLabel from "./Label/index";
const modules = {
  crmGlobal,
  crmDashboard,
  crmAuthUser,
  crmAuthRole,
  crmLogin,
  crmCopy,
  crmEdit,
  crmQuestion,
  crmLabel,
};

export default modules;
