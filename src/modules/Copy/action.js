/** @format */

import {
  apiGetDataList,
  apiGetInfoData,
  apiAddData,
  apiEditData,
  apiDeleteData,
} from "./api";

export const actions = {
  async actionGetDataList({ commit }, postData) {
    try {
      const resData = await apiGetDataList(postData);
      return resData.data;
    } catch (error) {
      return error;
    }
  },
  async actionGetInfoData({ commit }, postData) {
    try {
      const resData = await apiGetInfoData(postData);
      return resData.data;
    } catch (error) {
      return error;
    }
  },
  async actionAddData({ commit }, postData) {
    try {
      const resData = await apiAddData(postData);
      return resData.data;
    } catch (error) {
      return error;
    }
  },
  async actionEditData({ commit }, postData) {
    try {
      const resData = await apiEditData(postData);
      return resData.data;
    } catch (error) {
      return error;
    }
  },
  async actionDeleteData({ commit }, postData) {
    try {
      const resData = await apiDeleteData(postData);
      return resData.data;
    } catch (error) {
      return error;
    }
  },
};
