/** @format */

import { actions } from "./action"

export default {
  actions,
  namespaced: true
}
