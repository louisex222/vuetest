/** @format */

import { backendService } from "@utils/request";

export function apiGetDataList(postData) {
  return backendService({
    url: "/authAccount/getList",
    method: "POST",
    data: postData,
  });
}

export function apiGetInfoData(postData) {
  return backendService({
    url: "/authAccount/getInfo",
    method: "POST",
    data: postData,
  });
}

export function apiAddData(postData) {
  return backendService({
    url: "/authAccount/add",
    method: "POST",
    data: postData,
  });
}

export function apiEditData(postData) {
  return backendService({
    url: "/authAccount/edit",
    method: "POST",
    data: postData,
  });
}

export function apiDeleteData(postData) {
  return backendService({
    url: "/authAccount/delete",
    method: "POST",
    data: postData,
  });
}
