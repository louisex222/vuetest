/** @format */
import { defineComponent } from "vue";
import { createNamespacedHelpers } from "vuex";
const { mapActions } = createNamespacedHelpers("crmAuthAccount");
export default defineComponent({
  methods: {
    ...mapActions([
      "actionGetDataList",
      "actionGetInfoData",
      "actionAddData",
      "actionEditData",
      "actionDeleteData",
    ]),
  },
});
