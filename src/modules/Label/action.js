/** @format */

import {
  apiGetDataList,
  apiGetInfoData,
  apiAddData,
  apiEditData,
  apiDeleteData,
} from "./api";

export const actions = {
  // eslint-disable-next-line no-unused-vars
  async actionGetDataList({ commit }, postData) {
    try {
      console.log(postData);
      const resData = await apiGetDataList(postData);
      return {
        code: 200,
        data: {
          total: 30,
          list: [
            {
              id: 6100326568486,
              name: "毛濤",
              account: "HBCU3",
              role: 1,
              email: "Ffghfgjko",
              create: "2009-10-30",
              sign: "1993-06-13",
              state: false,
            },
            {
              id: 153215613461,
              name: "潭平",
              account: "HBCU3",
              email: 9.0,
              role: 1,
              create: "2009-10-30",
              sign: "1993-06-13",
              state: true,
            },
            {
              id: 153215613462,
              name: "周竟",
              account: "HBCU3",
              email: 16.0,
              role: 1,
              create: "2009-10-30",
              sign: "1993-06-13",
              state: true,
            },
            {
              id: 153215613463,
              name: "博平",
              account: "HBCU3",
              email: 3.7,
              role: 1,
              create: "2009-10-30",
              sign: "1993-06-13",
              state: true,
            },
            {
              id: 153215613464,
              name: "彭用",
              account: "HBCU3",
              email: 16.0,
              role: 1,
              create: "2009-10-30",
              sign: "1993-06-13",
              state: true,
            },
            {
              id: 153215613465,
              name: "李羅",
              account: "HBCU3",
              email: 0.0,
              role: 1,
              create: "2009-10-30",
              sign: "1993-06-13",
              state: true,
            },
            {
              id: 153215613466,
              name: "張鎮",
              account: "HBCU3",
              email: 0.2,
              role: 1,
              create: "2009-10-30",
              sign: "1993-06-13",
              state: true,
            },
            {
              id: 153215613467,
              name: "葉問",
              account: "HBCU3",
              email: 3.2,
              role: 1,
              create: "2009-10-30",
              sign: "1993-06-13",
              state: true,
            },
            {
              id: 153215613468,
              name: "葉準",
              account: "HBCU3",
              email: 25.0,
              role: 1,
              create: "2009-10-30",
              sign: "1993-06-13",
              state: true,
            },
            {
              id: 153215613469,
              name: "永春",
              account: "HBCU3",
              email: 26.0,
              role: 1,
              create: "2009-10-30",
              sign: "1993-06-13",
              state: true,
            },
            {
              id: 153215613453,
              name: "潭平",
              account: "HBCU3",
              email: 25.0,
              role: 1,
              create: "2009-10-30",
              sign: "1993-06-13",
              state: true,
            },
            {
              id: 153215613450,
              name: "葉正",
              account: "HBCU3",
              email: 26.0,
              role: 1,
              create: "2009-10-30",
              sign: "1993-06-13",
              state: true,
            },
            {
              id: 153215613473,
              name: "泰森",
              account: "HBCU3",
              email: 25.0,
              role: 1,
              create: "2009-10-30",
              sign: "1993-06-13",
              state: true,
            },
            {
              id: 153215613499,
              name: "西瓜",
              account: "HBCU3",
              email: 26.0,
              role: 1,
              create: "2009-10-30",
              sign: "1993-06-13",
              state: true,
            },
          ],
        },
      };
    } catch (error) {
      return error;
    }
  },
  // eslint-disable-next-line no-unused-vars
  async actionGetInfoData({ commit }, postData) {
    try {
      const resData = await apiGetInfoData(postData);
      return {
        code: 200,
        data: {
          id: 1,
          name: "毛濤",
          account: "HBCU3",
          email: "Ffghfgjko",
          role: 1,
          create: "2009-10-30",
          sign: "1993-06-13",
          state: false,
        },
      };
    } catch (error) {
      return error;
    }
  },
  // eslint-disable-next-line no-unused-vars
  async actionAddData({ commit }, postData) {
    try {
      const resData = await apiAddData(postData);
      return resData;
    } catch (error) {
      return error;
    }
  },
  // eslint-disable-next-line no-unused-vars
  async actionEditData({ commit }, postData) {
    try {
      const resData = await apiEditData(postData);
      return resData;
    } catch (error) {
      return error;
    }
  },
  // eslint-disable-next-line no-unused-vars
  async actionDeleteData({ commit }, postData) {
    try {
      const resData = await apiDeleteData(postData);
      return resData;
    } catch (error) {
      return error;
    }
  },
};
