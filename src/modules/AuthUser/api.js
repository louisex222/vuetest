/** @format */

import { api } from "boot/axios";
export function apiGetDataList(postData) {
  return api({
    url: "/authUser",
    method: "GET",
    params: postData,
  });
}

export function apiGetInfoData(postData) {
  return api({
    url: "/authUser/" + postData.id,
    method: "GET",
  });
}

export function apiAddData(postData) {
  return api({
    url: "/authUser",
    method: "POST",
    data: postData,
  });
}

export function apiEditData(postData) {
  return api({
    url: "/authUser/" + postData.id,
    method: "PUT",
    data: postData,
  });
}

export function apiDeleteData(postData) {
  return api({
    url: "/authUser/" + postData.id,
    method: "DELETE",
  });
}

export function apiRoleList(postData) {
  return api({
    url: "/roles",
    method: "GET",
    params: postData,
  });
}
