import MainLayout from "@layouts/MainLayout.vue";
const routes = [
  {
    path: "/",
    name: "/",
    meta: { requiresAuth: true },
    component: MainLayout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "dashboard",
        component: () => import("pages/Dashboard.vue"),
      },
    ],
  },
  {
    path: "/auth",
    name: "auth",
    meta: { requiresAuth: true },
    component: MainLayout,
    children: [
      {
        path: "authUser",
        name: "authUser",
        component: () => import("pages/AuthUser.vue"),
      },
      {
        path: "authRole",
        name: "authRole",
        component: () => import("pages/AuthRole.vue"),
      },
    ],
  },
  {
    path: "/copy",
    name: "copy",
    meta: { requiresAuth: true },
    component: MainLayout,
    children: [
      {
        name: "copyTest",
        path: "copyTest",
        component: () => import("pages/Copy.vue"),
      },
    ],
  },
  {
    path: "/question",
    name: "question",
    meta: { requiresAuth: false },
    component: MainLayout,
    children: [
      {
        path: "question1",
        name: "question1",
        component: () => import("pages/Question.vue"),
      },
      {
        path: "label",
        name: "label",
        component: () => import("pages/Label.vue"),
      },
    ],
  },
  {
    path: "/login",
    name: "login",
    meta: { requiresAuth: false },
    component: () => import("pages/Login.vue"),
  },
  {
    path: "/edit",
    name: "edit",
    component: () => import("pages/Edit.vue"),
  },
];

export default routes;
