/** @format */

import axios from "axios";
const backendService = axios.create({
  baseURL: process.env.DEV_API_URL,
});
// request interceptor
backendService.interceptors.request.use(
  (config) => {
    let addOption = {
      // lang: getLocalStorage("lang"),
    };
    config.data = Object.assign(config.data || {}, addOption);

    return config;
  },
  (error) => {
    // Do something with request error
    Promise.reject(error);
  }
);

// response interceptor
backendService.interceptors.response.use(
  async (response) => {
    return response;
  },
  async (error) => {
    return error;
  }
);
export { backendService };
