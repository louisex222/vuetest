import enUS from "./en-US/index";
import zhTW from "./zh-TW/index";
export default {
  "zh-TW": zhTW,
  "en-US": enUS,
};
