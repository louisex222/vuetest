module.exports = {
  id: "號碼",
  name: "題目",
  category: "分類名稱",
  feature: "功能",
  label: "標籤",
  state: "狀態",
  sort: "排序編號",
  operate: "操作",
  mangage: "管理名稱",
  time: "時間",
  ip: "IP位置",
  explanation: "說明",
};
