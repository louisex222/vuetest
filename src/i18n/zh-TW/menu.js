module.exports = {
  dashboard: "公布欄",
  auth: "系統管理",
  label: "標籤",
  authUser: "帳戶權限",
  authRole: "角色權限",
  question: "問題",
  question1: "問題選單",
  edit: "編輯器",
};
