/** @format */
import { initUser } from "./mutations.js";
const state = {
  root: {
    user: Object.assign({}, initUser),
    system: {
      navFlag: false, //側欄開關
    },
  },
};
export default state;
