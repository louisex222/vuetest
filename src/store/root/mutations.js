/** @format */
import * as types from "./mutationsTypes";
export const initUser = {
  // 使用者資訊
  user_name: "", //使用者名稱
};
export const mutations = {
  [types.GLOBAL_CHANGE_NAVFLAG](state) {
    let globalSys = state.root.system;
    globalSys.navFlag = !globalSys.navFlag;
  },
  [types.GLOBAL_LOGIN](state, resData) {
    let globalUser = state.root.user;
    globalUser.isLogin = true;
  },
};
