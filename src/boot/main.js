import { boot } from "quasar/wrappers";
import { getLocalStorage } from "@utils/method.js";
export default boot(({ app, router }) => {
  router.beforeEach(async (to, from, next) => {
    //切維護頁時會發生chunk error因此重整導入維護頁
    router.onError((error) => {
      const pattern = /Loading chunk (\d)+ failed/g;
      const csspattern = /Loading CSS chunk (\d)+ failed/g;
      const isChunkLoadFailed = error.message.match(pattern);
      const iscssChunkLoadFailed = error.message.match(csspattern);
      if (isChunkLoadFailed || iscssChunkLoadFailed) {
        window.location.reload();
      }
    });
    // 登入機制檢查
    let token = getLocalStorage("access_token");
    if (to.meta.requiresAuth && !token) {
      next({ path: "/login" });
    } else {
      next();
    }
  });
});
