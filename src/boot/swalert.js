import { boot } from "quasar/wrappers";
import Sweetalert from "sweetalert2";
//ㄧ個按鈕
const swal = Sweetalert.mixin({
  showCloseButton: true,
  allowOutsideClick: false,
  showCancelButton: false,
  showConfirmButton: true,
  title: "提示",
  confirmButtonText: "OK",
});
//兩個確認按鈕
const swal2 = Sweetalert.mixin({
  showCloseButton: true,
  allowOutsideClick: false,
  showCancelButton: true,
  showConfirmButton: true,
  title: "提示",
  confirmButtonText: "OK",
  cancelButtonText: "Cancel",
});
//無按鈕
const swal3 = Sweetalert.mixin({
  showCloseButton: false,
  allowOutsideClick: false,
  showCancelButton: false,
  showConfirmButton: false,
  title: "提示",
  timer: 2000,
});
export default boot(({ app }) => {
  app.config.globalProperties.$swal = swal;
});
export { swal, swal2, swal3 };
